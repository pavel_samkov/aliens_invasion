class Settings():
	"""Class for saving all settings of the game"""
	def __init__(self):
		"""Initialize settings of the game"""
		#Parameters of the screen
		self.screen_width = 1200
		self.screen_height = 800
		self.bg_color = (230, 230, 230)

		#Parameters of a ship
		
		self.ships_limit = 3

		#Parameters of bullet
		self.bullet_width = 3000
		self.bullet_height = 15
		self.bullet_color = (60, 60, 60)
		self.bullets_allowed = 3

		#alien settings
		self.fleet_drop_speed = 10
		

		#Tempo accelerating game 
		self.speedup_scale = 1.1
		self.score_scale = 1.5

		self.initialize_dynamic_settings()

	def initialize_dynamic_settings(self):
		"""Initialize settings, that changes in game"""
		self.ship_speed = 1.5
		self.bullet_speed = 3.0
		self.alien_speed = 1.0
		self.alien_points = 50

		#fleet direction = 1 means moving right
		#fleet direction = -1 mean moving left
		self.fleet_direction = 1 

	def increase_speed(self):
		"""Increase speed settings"""
		self.ship_speed *= self.speedup_scale
		self.bullet_speed *= self.speedup_scale
		self.alien_speed *= self.speedup_scale
		self.alien_points = int(self.alien_points * self.score_scale)


