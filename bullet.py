import pygame 
from pygame.sprite import Sprite 

class Bullet(Sprite):
	"""Class for managing bullets"""

	def __init__(self, ai_game):
		"""Create object of bullet in current position of the ship"""
		super().__init__()
		self.screen = ai_game.screen
		self.settings = ai_game.settings
		self.color = self.settings.bullet_color

		#Creating bullet in pos (0, 0) and set right pos
		self.rect = pygame.Rect(0, 0, self.settings.bullet_width,
			self.settings.bullet_height)
		self.rect.midtop = ai_game.ship.rect.midtop

		#pos of bullet saving in float
		self.y = float(self.rect.y)

	def update(self):
		"""Move bullet up"""
		self.y -= self.settings.bullet_speed
		#Update pos of rectangle
		self.rect.y = self.y

	def draw_bullet(self):
		"""Draw the bullet"""
		pygame.draw.rect(self.screen, self.color, self.rect)