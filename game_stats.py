class GameStats():
	"""Track statisctics of the game"""
	def __init__(self, ai_game):
		"""Initialize stats"""
		self.settings = ai_game.settings
		self.reset_stats()

		#game starts from 'non-active' status
		self.game_active = False
		self.high_score = 0
		
	def reset_stats(self):
		"""Initialize stats"""
		self.ships_left = self.settings.ships_limit
		self.score = 0
		self.level = 1